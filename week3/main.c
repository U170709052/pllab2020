#include<stdio.h>
#include<stdlib.h>
int nrDigits(int num)
{
    static int count=0;
    if(num>0)
    {
        count++;
        nrDigits(num/10);
    }
    else
        return count;
}

int main()
{
    int num="2123124";
    int result=nrDigits(num);
    printf(" digits in %d is %d\n",num,result);

    return(0);
}
